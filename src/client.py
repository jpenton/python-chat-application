from socket import *
import tkinter.messagebox as mb
from tkinter import *
from codecs import decode


HOST = "localhost"
PORT = 5000
BUFSIZE = 1024
ADDRESS = (HOST, PORT)
CODE = "ascii"


class Client:
    def __init__(self):
        self.socket = socket(AF_INET, SOCK_STREAM)
        self.window_main = Tk()
        self.label_main = Label(master=self.window_main, text="Enter message:")
        self.entry_main = Entry(master=self.window_main)
        self.connect_string = None
        self.button_reply = Button(
            master=self.window_main,
            text="Send Reply",
            command=self.send_reply,
            state=DISABLED,
        )
        self.button_connect = Button(
            master=self.window_main, text="Connect", command=self.connect
        )

        self.label_main.grid(row=0, columnspan=2)
        self.entry_main.grid(row=1, columnspan=2)
        self.button_reply.grid(row=2, column=0)
        self.button_connect.grid(row=2, column=1)

        mainloop()

    def connect(self):
        self.socket.connect(ADDRESS)
        self.button_reply.config(state=NORMAL)

    def send_reply(self):
        msg = self.entry_main.get()
        self.socket.send(bytes(msg, CODE))
        reply = decode(self.socket.recv(BUFSIZE), CODE)
        mb.showinfo(title="New Message", message=reply)


cli = Client()
